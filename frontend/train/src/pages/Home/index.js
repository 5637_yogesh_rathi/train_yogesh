import { Link } from 'react-router-dom'

import { toast } from 'react-toastify'
import axios from 'axios'
import { useEffect, useState } from 'react'
//import { useNavigate } from 'react-router'
import { URL } from '../../config'
import SearchTrain from '../../components/Search train/SearchTrain'
import SearchTrain1 from '../../components/userHome1/userHome1'

const Home = () => {

  const {id,firstName,lastName} = sessionStorage
  const{source,destination,date}= sessionStorage
  //const navigate = useNavigate()

  const [train, setTrain] = useState([])


  const searchTrain =()=>{
const url = `${URL}/train/${source}/${destination}/${date} `
axios.post(url).then((response) => {
  const result = response.data
  if (result['status'] == 'success') {
    setTrain(result['data'])
  } else {
    toast.error(result['error'])
  }
})
  
// useEffect(() => {
  //searchTrain()
   // console.log('getting called')
  //}, [])

}
  return (
    <div>
      <h1>Home</h1>
      <div>
        <h1>Welcome {firstName}</h1>
      </div>
      <SearchTrain1/>

      
    </div>
  )
  }

export default Home