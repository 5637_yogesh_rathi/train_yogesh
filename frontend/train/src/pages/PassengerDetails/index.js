import { Link } from 'react-router-dom'
import { useState } from 'react'
import { toast } from 'react-toastify'
import axios from 'axios'
import { useNavigate } from 'react-router'
import { useAbsoluteLayout } from 'react-table'


const Addpassenger = () => {

    const[ name , setName ] = useState('')
    const[  gender, setGender ] = useState('')
    const[ phone , setPhone ] = useState('')
    const[ region , setRegion ] = useState('')
    const[ age , setAge ] = useState('')

    const {id} = sessionStorage
    const {trainNo} = sessionStorage
    const {trainDate} = sessionStorage
    
    
    const navigate = useNavigate()
    const addpassenger = ()=>{

        if (name.length == 0) {
            toast.warning('Please enter  name')
          } else if (gender.length == 0) {
            toast.warning('Please enter gender')
          } else if (phone.length == 0) {
            toast.warning('Please enter phone no')
          }  else if (region.length == 0) {
            toast.warning('Please enter region')
          } else if (age.length == 0) {
            toast.warning('Please enter age')
          } else {
            const body = {
                name,
                gender,
                phone,
                region,
                age,

            }

            const url  = `http://localhost:8080/bookTicket/${trainNo}/${trainDate}/${id}`

            axios.post(url,body).then((response)=>{
                const result = response.data 
                console.log(result)
                if (result['status'] == 'success') {
                    toast.success('passenger added succefully')
                    navigate('/home')

                  } 
                  else {
                    toast.error(result['error'])
                  }

            })
    }

    }
    
    return(
        <div>
           <div>
    <h1  className="title">Add Passenger</h1>

    <div className="row">
      <div className="col"></div>
      <div className="col">
        <div className="form">
          <div className="mb-3">
            <label htmlFor="" className="label-control">
              Name
            </label>
            <input
              onChange={(e) => {
                setName(e.target.value)
              }}
              type="text"
              className="form-control"
            />
          </div>

          <div className="mb-3">
            <label htmlFor="" className="label-control">
              Gender
            </label>
            <input
              onChange={(e) => {
                setGender(e.target.value)
              }}
              type="text"
              className="form-control"
            />
          </div>

          <div className="mb-3">
            <label htmlFor="" className="label-control">
              Phone No 
            </label>
            <input
              onChange={(e) => {
                setPhone(e.target.value)
              }}
              type="text"
              className="form-control"
            />
          </div>

          <div className="mb-3">
            <label htmlFor="" className="label-control">
              Region
            </label>
            <input
              onChange={(e) => {
                setRegion(e.target.value)
              }}
              type="text"
              className="form-control"
            />
          </div>

          <div className="mb-3">
            <label htmlFor="" className="label-control">
              Age
            </label>
            <input
              onChange={(e) => {
                setAge(e.target.value)
              }}
              type="text"
              className="form-control"
            />
          </div>

          <div className="mb-3">
            
            <button onClick={addpassenger} className="btn btn-primary">
              Book
            </button>
          </div>
        </div>
      </div>
      <div className="col"></div>
    </div>
  </div>
        </div>
    )
}

export default Addpassenger;