
import { Link } from 'react-router-dom'
import {useState} from 'react'
import axios from 'axios'

import {toast} from 'react-toastify'
import { useEffect } from 'react'
import { useLocation } from 'react-router'
import Train from '../../components/train'
import { useNavigate } from 'react-router'


const AdminHome = () =>{
    
    const [trainNo, setTrainNo] = useState('')
    const navigate = useNavigate()

        const [trainarray, setTrainArray] = useState([])

        const searchTrainByNo = ()=>{
            if (trainNo.length == 0) {
                toast.warning('please enter Train number')
              } else
               {
                const body = {
                  trainNo,
                  
                }

            const url = `http://localhost:8080/train/trainByNo/${trainNo}`
            axios.get(url).then((response) => {
            const result = response.data
            if (result['status'] == 'success') {
                setTrainArray(result [ 'data' ] );
                console.log(result)
            } else {
            toast.error(result['error'])
      }
    })
        }
    }
    function onBookHandler(trainData) {
        sessionStorage.setItem('trainData' , trainData);
        let isLogin = sessionStorage.getItem('loginStatus');
        sessionStorage['trainNo'] = trainData.no;
        if(isLogin){
          navigate('/passengerdetails')
        }else{
          navigate('/signin');
        }
      }
        
    return(
        <div>
        <meta charSet="utf-8" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, shrink-to-fit=no"
        />
        <title>UserHome</title>
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/fonts/font-awesome.min.css" />
        <link rel="stylesheet" href="assets/fonts/ionicons.min.css" />
        <link
          rel="stylesheet"
          href="assets/css/Material-Style-Ripple-Button.css"
        />
        <link rel="stylesheet" href="assets/css/Button-Outlines---Pretty.css" />
        <link rel="stylesheet" href="assets/css/Footer-Basic.css" />
        <link rel="stylesheet" href="assets/css/styles.css" />
        <div className="container">
          <div className="row" />
        </div>
        <div className="container cool-btn-container">
          <div className="row">
            <div className="col">
              <div className="form-group mb-3">
                <label className="form-label">Train Number</label>
                <input onChange={(e) => {
                  setTrainNo(e.target.value)
                }} type="text" className="form-control" />
              </div>
           
              <button onClick={searchTrainByNo} className="btn btn-info" type="button">
                Search Train&nbsp;
                <i className="icon ion-android-arrow-forward" />
              </button>
            </div>
            <div
              className="col-lg-2 d-flex flex-column py-3"
              style={{ margin: "0 auto" }}
            >
              <button className="btn btn-primary" type="button">
                Home
              </button>
              <br />
              <button className="btn btn-info" type="button">
                Profile
              </button>
              <br />
              <button className="btn btn-warning" type="button">
                Add Train
              </button>
              <br />
              <button className="btn btn-danger" type="button">
                Add Admin
              </button>
              <br />
              
              <button className="btn btn-dark" type="button">
                Logout
              </button>
              <br />
            </div>
          </div>
          <div class="row">
        <table>
        <tr>
        <th>Train No</th>
        <th>TrainName</th>
        <th>Source</th>
          <th>Arrival Time</th>
          <th>Departure Time</th>
          <th>Destination</th>
          <th>TotalSeats</th>
          <th>Fair</th>
        </tr>
        {trainarray.map((val, key) => {
          return (
            <tr key={key}>
              <td>{val.no}</td>
              <td>{val.trainName}</td>
              <td>{val.source}</td>
              <td>{val.arrivalTime}</td>
              <td>{val.departureTime}</td>
              <td>{val.destination}</td>
              <td>{val.totalSeats}</td>
              <td>{val.fair}</td>
              <td>
                <button onClick={() => onBookHandler(val)}>Book</button>
              </td>
            </tr>
          )
        })}
      </table>  
        </div>
        </div>
      </div>
     

    )
}

export default AdminHome
