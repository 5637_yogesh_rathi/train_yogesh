import Signup from './pages/Signup';
import { BrowserRouter, Route, Routes, Link } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import Signin from './pages/Signin';
import Home from './pages/Home';
import TrainDetails from './pages/TrainDetails';
import PassangerDetails from './pages/PassengerDetails';
import AdminSignin from './pages/AdminSignin';
import User from './pages/UserHome';
import Home1 from './pages/Home1';
import UpdateProfile from './pages/UserProfile';
import AdminHome from './pages/AdminHome';

const AuthorizeUser = () => {
  const loginStatus = sessionStorage['loginStatus']
  return loginStatus == '1' ? <Home /> : <Signin />
  // if (loginStatus == '1') {
  //   return <Home />
  // } else {
  //   return <Signin />
  // }
}


function App() {
  return (
    <div className="container">
      <BrowserRouter>
        <Routes>
        <Route path="/" element={<AuthorizeUser />} />
        <Route path="/signin" element={<Signin />} />
        <Route path="/signup" element={<Signup />} />
        <Route path="/home" element={<Home/>} />
        <Route path="/trainDetails" element={<TrainDetails/>} />
        <Route path="/passengerdetails" element={<PassangerDetails/>} />
        <Route path="/adminsignin" element={<AdminSignin/>} />
        <Route path="/userhome" element={<User/>} />
        <Route path="/home1" element={<Home1/>} />
        <Route path="/updateProfile" element={<UpdateProfile/>} />
        <Route path="/adminHome" element={<AdminHome/>} />
        </Routes>
      </BrowserRouter>
      <ToastContainer theme="colored" />
    </div>
  );
}

export default App;