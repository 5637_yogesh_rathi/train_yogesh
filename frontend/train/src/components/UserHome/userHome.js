import { useState } from 'react'
import { Link } from 'react-router-dom'

import { toast } from 'react-toastify'
import axios from 'axios'
import { useNavigate } from 'react-router'
import Train from '../train'
import ReactTable from "react-table";  
import PassangerDetails from '../Passanger Details/passanger.details'


const SearchTrain = () => {
  const [source, setSource] = useState('')
  const [destination, setDestination] = useState('')
  const [date, setDate] = useState('')
  const {firstname,lastname} = sessionStorage
  const navigate = useNavigate()
  const [traindetails,setTraindetails]=useState([])

 

  const searchTrain =  () => {
    if (source.length == 0) {
      toast.warning('please enter email')
    } else if (destination.length == 0) {
      toast.warning('please enter password')
    }else if (destination.length == 0) {
    toast.warning('please enter password')
    }else {
      const body = {
        source,
        destination,
        date
      }

      const url = `http://localhost:8080/train/search`

      // make api call using axios
      axios.post(url, body).then((response) => {
        const result = response.data
        console.log(result)
        if (result['status'] == 'success') {
            setTraindetails(result [ 'data' ] );
  
      const trainNo = result.data[0].no
      const trainDate = date;
      //sessionStorage['trainNo'] = trainNo
      sessionStorage['trainDate']=trainDate
              

       //  const { traindetails } = result['data']
        //   console.log(traindetails.trainNo);
            
            
            //navigate('/trainDetails', {state : {traindetails : traindetails}})
        } else {
          toast.error(result['error'])
        }

      })
    }
  }

  function onBookHandler(trainData) {
    sessionStorage.setItem('trainData' , trainData);
    let isLogin = sessionStorage.getItem('loginStatus');
    sessionStorage['trainNo'] = trainData.no;
    if(isLogin){
      navigate('/passengerdetails')
    }else{
      navigate('/signin');
    }
  }

 

 

  return (
    <div>

      <div className="row">
        <div className="col">
          <div className="form">
            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Source
              </label>
              <input
                onChange={(e) => {
                  setSource(e.target.value)
                }}
                type="text"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Destination
              </label>
              <input
                onChange={(e) => {
                  setDestination(e.target.value)
                }}
                type="text"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Destination
              </label>
              <input
                onChange={(e) => {
                  setDate(e.target.value)
                }}
                type="date"  data-date-format="YYYY-MM-DD"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <button onClick={searchTrain} className="btn btn-primary">
                Search
              </button>
            </div>
         
          </div>
        </div>
        <div class="row">
        <table>
        <tr>
        <th>Train No</th>
        <th>TrainName</th>
        <th>Source</th>
          <th>Arrival Time</th>
          <th>Departure Time</th>
          <th>Destination</th>
          <th>TotalSeats</th>
          <th>Fair</th>
        </tr>
        {traindetails.map((val, key) => {
          return (
            <tr key={key}>
              <td>{val.no}</td>
              <td>{val.trainName}</td>
              <td>{val.source}</td>
              <td>{val.arrivalTime}</td>
              <td>{val.departureTime}</td>
              <td>{val.destination}</td>
              <td>{val.totalSeats}</td>
              <td>{val.fair}</td>
              <td>
                <button onClick={() => onBookHandler(val)}>Book</button>
              </td>
            </tr>
          )
        })}
      </table>  
        </div>
      </div>
    </div>
    
  )
}

export default SearchTrain