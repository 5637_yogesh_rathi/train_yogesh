import { useState } from "react";
import { Link } from "react-router-dom";

import { toast } from "react-toastify";
import axios from "axios";
import { useNavigate } from "react-router";
import Train from "../train";
import ReactTable from "react-table";
import PassangerDetails from "../Passanger Details/passanger.details";

const SearchTrain1 = () => {
  const [source, setSource] = useState("");
  const [destination, setDestination] = useState("");
  const [date, setDate] = useState("");
  const { firstname, lastname } = sessionStorage;
  const navigate = useNavigate();
  const [traindetails, setTraindetails] = useState([]);

  const searchTrain = () => {
    if (source.length == 0) {
      toast.warning("please enter email");
    } else if (destination.length == 0) {
      toast.warning("please enter password");
    } else if (destination.length == 0) {
      toast.warning("please enter password");
    } else {
      const body = {
        source,
        destination,
        date,
      };

      const url = `http://localhost:8080/train/search`;

      // make api call using axios
      axios.post(url, body).then((response) => {
        const result = response.data;
        console.log(result);
        if (result["status"] == "success") {
          setTraindetails(result["data"]);

          const trainNo = result.data[0].no;
          const trainDate = date;
          //sessionStorage['trainNo'] = trainNo
          sessionStorage["trainDate"] = trainDate;

          //  const { traindetails } = result['data']
          //   console.log(traindetails.trainNo);

          //navigate('/trainDetails', {state : {traindetails : traindetails}})
        } else {
          toast.error(result["error"]);
        }
      });
    }
  };

  function onBookHandler(trainData) {
    sessionStorage.setItem("trainData", trainData);
    let isLogin = sessionStorage.getItem("loginStatus");
    sessionStorage["trainNo"] = trainData.no;
    if (isLogin) {
      navigate("/passengerdetails");
    } else {
      navigate("/signin");
    }
  }
  const logoutUser = () => {
    // remove the logged users details from session storage
    sessionStorage.removeItem('id')
    sessionStorage.removeItem('firstName')
    sessionStorage.removeItem('lastName')
    sessionStorage.removeItem('loginStatus')

    // navigate to sign in component
    navigate('/signin')
  }
  const navigatetoUserProfile=()=>{
      navigate("/updateProfile")
  }

  return (
    <div>
      <meta charSet="utf-8" />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1.0, shrink-to-fit=no"
      />
      <title>UserHome</title>
      <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" />
      <link rel="stylesheet" href="assets/fonts/font-awesome.min.css" />
      <link rel="stylesheet" href="assets/fonts/ionicons.min.css" />
      <link
        rel="stylesheet"
        href="assets/css/Material-Style-Ripple-Button.css"
      />
      <link rel="stylesheet" href="assets/css/Button-Outlines---Pretty.css" />
      <link rel="stylesheet" href="assets/css/Footer-Basic.css" />
      <link rel="stylesheet" href="assets/css/styles.css" />
      <div className="container">
        <div className="row" />
      </div>
      <div className="container cool-btn-container">
        <div className="row">
          <div className="col">
            <div className="form-group mb-3">
              <label className="form-label">Source</label>
              <input
                onChange={(e) => {
                  setSource(e.target.value);
                }}
                type="text"
                className="form-control"
              />
            </div>
            <div className="form-group mb-3">
              <label className="form-label">Destination</label>
              <input
                onChange={(e) => {
                  setDestination(e.target.value);
                }}
                type="text"
                className="form-control"
              />
            </div>
            <div className="form-group mb-3">
              <label className="form-label">Date</label>
              <input
                onChange={(e) => {
                  setDate(e.target.value);
                }}
                type="date"
                className="form-control"
              />
            </div>
            <button
              onClick={searchTrain}
              className="btn btn-info"
              type="button"
            >
              Search Train&nbsp;
              <i className="icon ion-android-arrow-forward" />
            </button>
          </div>
          <div
            className="col-lg-2 d-flex flex-column py-3"
            style={{ margin: "0 auto" }}
          >
            <button className="btn btn-primary" type="button">
              Home
            </button>
            <br />
            <button onClick={navigatetoUserProfile}  className="btn btn-info" type="button">
              Profile
            </button>
            <br />
            <button className="btn btn-warning" type="button">
              My Transaction
            </button>
            <br />
            <button className="btn btn-danger" type="button">
              Cancel Ticket
            </button>
            <br />
            <button className="btn btn-dark" type="button">
              Ticket Status
            </button>
            <br />
            <button onClick={logoutUser} className="btn btn-dark" type="button">
              Logout
            </button>
            <br />
          </div>
          <div class="row">
            <table>
              <tr>
                <th>Train No</th>
                <th>TrainName</th>
                <th>Source</th>
                <th>Arrival Time</th>
                <th>Departure Time</th>
                <th>Destination</th>
                <th>TotalSeats</th>
                <th>Fair</th>
              </tr>
              {traindetails.map((val, key) => {
                return (
                  <tr key={key}>
                    <td>{val.no}</td>
                    <td>{val.trainName}</td>
                    <td>{val.source}</td>
                    <td>{val.arrivalTime}</td>
                    <td>{val.departureTime}</td>
                    <td>{val.destination}</td>
                    <td>{val.totalSeats}</td>
                    <td>{val.fair}</td>
                    <td>
                      <button onClick={() => onBookHandler(val)}>Book</button>
                    </td>
                  </tr>
                );
              })}
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SearchTrain1;
