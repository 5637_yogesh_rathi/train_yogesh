import axios from "axios"
import { useEffect } from "react"
import {toast} from 'react-toastify'
import { useNavigate } from "react-router"
import DeleteTrain from "../../pages/AdminHome"

const Train = (props)=>{
    const {train} = props
    const navigate = useNavigate()
    

const deleteTrain = ()=>{
    const trainno = sessionStorage['trainNo']
    
    const body={
        trainno,
    }
    const url = `http://localhost:8080/train/delete/${train.trainno}`
    axios.delete(url, body).then((response)=>{
        const result = response.data
            if (result['status'] == 'success') {
               toast.success('Train deleted successfull')
            } else {
            toast.error(result['error'])
        }
    })
    
}





    return(
    <div key={train.trainno} >
        <table className="table">
  <thead>
  </thead>
  <tbody>
    <tr>
      <th scope="row">{train.trainno}</th>
      <td>{train.name}</td>
      <td>{train.name}</td>
      {sessionStorage['trainno'] = train.trainno}
      <td><button key={train.trainno} onClick={deleteTrain}  >Delete</button></td>
      
    </tr>
    </tbody>
    </table>
    </div>
    )
}

export default Train